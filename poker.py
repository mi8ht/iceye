import argparse

from hands import convert_to_values, get_values_counts, validate_hand, compare_values

parser = argparse.ArgumentParser(description='Tells which hand in poker is upper')
parser.add_argument(
    'hand1',
    nargs="?",
    type=str,
    help='hand one string'
)
parser.add_argument(
    'hand2',
    nargs="?",
    type=str,
    help='hand two string'
)
args = parser.parse_args()


valid, message = validate_hand(args.hand1)
if not valid:
    print(message.format(arg="hand1"))

valid, message = validate_hand(args.hand2)
if not valid:
    print(message.format(arg="hand2"))

result = compare_values(
    get_values_counts(convert_to_values(args.hand1)),
    get_values_counts(convert_to_values(args.hand2))
)

if result == 0:
    print("It's a tie!")
elif result == 1:
    print("First hand wins!")
elif result == 2:
    print("Second hand wins!")
