# Python programming assignment

The goal of this assignment is to test your programming skills using Python. We hope to receive
an answer that is written in idiomatic Python3 and is easy to read, understand and modify. Use
those features of the language that make sense to you in this case.
You can share the code with us by by zipping up the code and attaching it to an email or by
sharing it via a private GitHub repository (in that case, invite github user tarvaina as a
collaborator).

#Specs

Given two poker hands determine which one wins. We use a simplified version of poker, where
the cards don't have suits and there are no flushes, straights or straight flushes. The two hands
are given as strings of five characters, where each character is one of 23456789TJQKA. The
answer should be "First hand wins!", "Second hand wins!" or "It's a tie!". A hand wins if it has a
more valuable combination than the other or if they have the same combination but the cards of
one are higher than the cards of the other.
The combinations are (in order of value):
- four of a kind (e.g. 77377)
- full house (e.g. KK2K2)
- triples (e.g. 32666)
- two pairs (e.g. 77332)
- pair (e.g. 43K9K)
- high card (i.e. anything else, e.g. 297QJ)

When the hands have the same combination, the winner is the one where the combination is
formed with higher cards. In the case of two pairs, the higher pair is compared first (e.g. 99662
wins 88776). In the case of full house, the triples are compared first and then the pair (e.g.
88822 wins QQ777). If the combinations are formed with the same cards, then the rest of the
cards are compared from the highest card to the lowest. E.g. when the hands are 7T2T6 and
TT753, the first one wins because TT=TT, 7=7 and 6>5.

#Examples
Each of these should print: It's a tie!

    python poker.py AAAQQ QQAAA
    python poker.py 53QQ2 Q53Q2
    python poker.py 53888 88385
    python poker.py QQAAA AAAQQ
    python poker.py Q53Q2 53QQ2
    python poker.py 88385 53888

Each of these should print: First hand wins!

    python poker.py AAAQQ QQQAA
    python poker.py Q53Q4 53QQ2
    python poker.py 53888 88375
    python poker.py 33337 QQAAA
    python poker.py 22333 AAA58
    python poker.py 33389 AAKK4
    python poker.py 44223 AA892
    python poker.py 22456 AKQJT
    python poker.py 99977 77799
    python poker.py 99922 88866
    python poker.py 9922A 9922K
    python poker.py 99975 99965
    python poker.py 99975 99974
    python poker.py 99752 99652
    python poker.py 99752 99742
    python poker.py 99753 99752

Each of these should print: Second hand wins!

    python poker.py QQQAA AAAQQ
    python poker.py 53QQ2 Q53Q4
    python poker.py 88375 53888
    python poker.py QQAAA 33337
    python poker.py AAA58 22333
    python poker.py AAKK4 33389
    python poker.py AA892 44223
    python poker.py AKQJT 22456
    python poker.py 77799 99977
    python poker.py 88866 99922
    python poker.py 9922K 9922A
    python poker.py 99965 99975
    python poker.py 99974 99975
    python poker.py 99652 99752
    python poker.py 99742 99752
    python poker.py 99752 99753
    
# Installation

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.txt
    
    
# Tests

    pytest test.py