import pytest

from hands import compare_values, get_values_counts, convert_to_values


@pytest.mark.parametrize("hand1, hand2, result", [
    ("AAAQQ", "QQAAA", 0),
    ("53QQ2", "Q53Q2", 0),
    ("53888", "88385", 0),
    ("QQAAA", "AAAQQ", 0),
    ("Q53Q2", "53QQ2", 0),
    ("88385", "53888", 0),
    ("AAAQQ", "QQQAA", 1),
    ("Q53Q4", "53QQ2", 1),
    ("53888", "88375", 1),
    ("33337", "QQAAA", 1),
    ("22333", "AAA58", 1),
    ("33389", "AAKK4", 1),
    ("44223", "AA892", 1),
    ("22456", "AKQJT", 1),
    ("99977", "77799", 1),
    ("99922", "88866", 1),
    ("9922A", "9922K", 1),
    ("99975", "99965", 1),
    ("99975", "99974", 1),
    ("99752", "99652", 1),
    ("99752", "99742", 1),
    ("99753", "99752", 1),
    ("QQQAA", "AAAQQ", 2),
    ("53QQ2", "Q53Q4", 2),
    ("88375", "53888", 2),
    ("QQAAA", "33337", 2),
    ("AAA58", "22333", 2),
    ("AAKK4", "33389", 2),
    ("AA892", "44223", 2),
    ("AKQJT", "22456", 2),
    ("77799", "99977", 2),
    ("88866", "99922", 2),
    ("9922K", "9922A", 2),
    ("99965", "99975", 2),
    ("99974", "99975", 2),
    ("99652", "99752", 2),
    ("99742", "99752", 2),
    ("99752", "99753", 2),
])
def test_compare_hands(hand1, hand2, result):
    assert result == compare_values(
        get_values_counts(convert_to_values(hand1)),
        get_values_counts(convert_to_values(hand2))
    )
