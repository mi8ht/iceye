from typing import List, Dict, Tuple


def validate_hand(hand: str) -> Tuple[bool, str]:
    if not hand:
        return False, "Argument {arg} can't be None type"

    if len(hand) != 5:
        return False, "Argument {arg} must be 5 character length string"

    hand = hand.upper()

    cards = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]

    for card in hand:
        if card not in cards:
            return False, "Argument {arg} must contain one of the card values" + " {cards}".format(cards=cards)

    return True, ""


def convert_to_values(hand: str) -> List[int]:
    """Convert hand string values to int values so they present actual strength of cards"""
    map_card_value = {
        "T": 10,
        "J": 11,
        "Q": 12,
        "K": 13,
        "A": 14,
    }

    hand = hand.upper()

    results = []
    for card in hand:
        if card in map_card_value:
            results.append(map_card_value[card])
        else:
            results.append(int(card))

    return results


def get_values_counts(hand: List[int]) -> List[Dict]:
    """
    Count how many times same value appears in hand and return list sorted from most powerful to least powerful
    combinations
    """
    values = {}
    for value in hand:
        key = str(value)
        values.setdefault(key, {"value": None, "count": 0})
        values[key]["value"] = value
        values[key]["count"] += 1

    # Get list of counts
    results = [i for _, i in values.items()]
    # Sort first by cards (their value representation)
    results.sort(key=lambda value: value["value"], reverse=True)
    # Then sort by number of same cards (their value representation)
    results.sort(key=lambda value: value["count"], reverse=True)

    return results


def compare_values(values1: List[Dict], values2: List[Dict]) -> int:
    """
    Compare hand's values (int representation) and return 0 if there's a tie, 1 if first hand wins 2 if second hand
    wins
    """
    length1 = len(values1)
    length2 = len(values2)
    length = length1 if length1 < length2 else length2

    for index in range(length):
        if values1[index]["count"] > values2[index]["count"]:
            return 1
        elif values1[index]["count"] < values2[index]["count"]:
            return 2
        else:
            # If first set of grouped cards in hand 1 have higher value than same size group of cards in hand 2, we
            # still need to check if there aren't any additional groups for losing hand (or at least what we think at
            # this point of comparision), because if there is, the value of whole hand is higher
            if values1[index]["value"] > values2[index]["value"] and length1 <= length2:
                return 1
            # Similar situation as above
            elif values1[index]["value"] < values2[index]["value"] and length1 >= length2:
                return 2

    return 0
